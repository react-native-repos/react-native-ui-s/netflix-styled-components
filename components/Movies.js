import React from "react";
import { View, Text, Dimensions } from "react-native";
import styled from "styled-components/native";

const Movies = ({ label, item }) => {
  return (
    <Container>
      <Label>{label}</Label>
      <MovieScroll horizontal>
        {item.map((movie, item) => (
          <MovieCard>
            <MoviePoster resizeMode="cover" source={movie} />
          </MovieCard>
        ))}
      </MovieScroll>
    </Container>
  );
};

export default Movies;

const Container = styled.View`
  padding: 20px 0;
`;
const Label = styled.Text`
  color: #fff;
  font-size: 16px;
  margin: 0 0 5px 10px;
`;
const MovieScroll = styled.ScrollView`
  padding-left: 10px;
`;
const MovieCard = styled.ScrollView`
  padding-right: 9px;
`;
const MoviePoster = styled.Image`
  width: ${(Dimensions.get("window").width * 28) / 100}px;
  height: 150px;
`;
